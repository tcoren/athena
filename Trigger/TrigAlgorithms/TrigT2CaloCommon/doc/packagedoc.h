/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/**

@page TrigT2CaloCommon_page 
@author Denis Oliveira
@author Carlos Osuna

@section TrigT2CaloCommon_TrigT2CaloCommonOverview Overview
This package provides data access classes and bytestream converters
objects for calorimeter reconstruction algorithms for L2 trigger.
In addition it contains classes for (EM) calibration and geometry helpers.



*/
