/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "TrigHLTJetHypoUnitTests/JetHypoExerciserAlg.h"
#include "TrigHLTJetHypoUnitTests/SimpleHypoJetVectorGenerator.h"

DECLARE_COMPONENT(JetHypoExerciserAlg)
DECLARE_COMPONENT(SimpleHypoJetVectorGenerator)
